package ora4mas.nopl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cartago.OPERATION;
import moise.common.MoiseException;
import moise.os.OS;
import moise.os.fs.Goal;
import moise.os.fs.Mission;
import moise.os.ns.Norm;
import moise.os.ss.Role;

public abstract class AccountabilityBoard extends OrgArt {

    protected Logger logger = Logger.getLogger(AccountabilityBoard.class.getName());

    protected OS os;

    // Agent - Goal - Provision
    private HashMap<String, HashMap<Goal, String>> acceptedProvisions;
    private HashMap<String, HashMap<Goal, String>> rejectedProvisions;

    // Agent - Roles
    private HashMap<String, List<Role>> acceptedPlayers;

    public void init(final String osFile) {

        os = OS.loadOSFromURI(osFile);

        oeId = getCreatorId().getWorkspaceId().getName();

        acceptedProvisions = new HashMap<>();
        rejectedProvisions = new HashMap<>();
        acceptedPlayers = new HashMap<>();

    }

    @OPERATION
    public void proposeProvisionForGoal(String goal, String provision) throws MoiseException {
        proposeProvisionForGoal(getOpUserName(), goal, provision);
    }
    private void proposeProvisionForGoal(String agent, String goal, String provision) throws MoiseException  {
        Goal g = os.getFS().findGoal(goal);
        Role r = findRoleWithGoal(g);
        List<Role> agentAcceptedRoles = acceptedPlayers.get(agent);
        if(agentAcceptedRoles != null && agentAcceptedRoles.contains(r)) {
            throw new MoiseException("The agent has been already accepted for this role. Provisions cannot be changed");
        }
        retractProvisionForGoal(agent, goal);
        if(isProvisionAccepted(g, provision)) {
            HashMap<Goal, String> agentAcceptedProvisions = acceptedProvisions.get(agent);
            if(agentAcceptedProvisions == null) {
                HashMap<Goal,String> ap = new HashMap<>();
                ap.put(g, provision);
                acceptedProvisions.put(agent, ap);
            }
            else {
                agentAcceptedProvisions.put(g, provision);
            }
            defineObsProperty("acceptedProvision", new JasonTermWrapper(agent), new JasonTermWrapper(goal), new JasonTermWrapper(provision));
        }
        else {
            HashMap<Goal, String> agentRejectedProvisions = rejectedProvisions.get(agent);
            if(agentRejectedProvisions == null) {
                HashMap<Goal,String> rp = new HashMap<>();
                rp.put(g, provision);
                rejectedProvisions.put(agent, rp);
            }
            else {
                agentRejectedProvisions.put(g, provision);
            }
            defineObsProperty("rejectedProvision", new JasonTermWrapper(agent), new JasonTermWrapper(goal), new JasonTermWrapper(provision));
        }
        
    }
    
    @OPERATION
    public void retractProvisionForGoal(String goal) throws MoiseException {
        retractProvisionForGoal(getOpUserName(), goal);
    }
    private void retractProvisionForGoal(String agent, String goal) {
        Goal g = os.getFS().findGoal(goal);
        HashMap<Goal, String> agentAcceptedProvisions = acceptedProvisions.get(agent);
        if(agentAcceptedProvisions != null) {
            String prov = agentAcceptedProvisions.get(g);
            if(prov != null) {
                agentAcceptedProvisions.remove(g);
                removeObsPropertyByTemplate("acceptedProvision", new JasonTermWrapper(agent), new JasonTermWrapper(goal), new JasonTermWrapper(prov));
            }
        }
        HashMap<Goal, String> agentRejectedProvisions = rejectedProvisions.get(agent);
        if(agentRejectedProvisions != null) {
            String prov = agentRejectedProvisions.get(g);
            if(prov != null) {
                agentRejectedProvisions.remove(g);
                removeObsPropertyByTemplate("rejectedProvision", new JasonTermWrapper(agent), new JasonTermWrapper(goal), new JasonTermWrapper(prov));
            }
        }
    }

    @OPERATION public void proposeForRole(String role) throws MoiseException {
        proposeForRole(getOpUserName(), role);
    }
    private void proposeForRole(String agent, String role) throws MoiseException {
        Role r = os.getSS().getRoleDef(role);
        List<Role> agentAcceptedRoles = acceptedPlayers.get(agent);
        if(agentAcceptedRoles != null && agentAcceptedRoles.contains(r)) {
            throw new MoiseException("The agent has been already accepted for this role.");
        }
        HashMap<Goal,String> agentRejectedProvisions = rejectedProvisions.get(agent);
        if(agentRejectedProvisions != null && !agentRejectedProvisions.isEmpty()) {
            Collection<Goal> roleGoals  = getRoleGoals(r);
            for(Goal g : roleGoals) {
                if(agentRejectedProvisions.containsKey(g)) {
                    return;
                }
            }
        }
        defineObsProperty("playerAccepted", new JasonTermWrapper(agent), new JasonTermWrapper(role));
        if(agentAcceptedRoles == null) {
            agentAcceptedRoles = new ArrayList<>();
            acceptedPlayers.put(agent, agentAcceptedRoles);
        }
        agentAcceptedRoles.add(r);
    }
    
    protected abstract boolean isProvisionAccepted(Goal goal, String provision);
    
    private Role findRoleWithGoal(Goal goal) throws MoiseException {
        Collection<Mission> missions = os.getFS().getAllMissions();
        Mission missionContainingGoal = null;
        for(Mission m : missions) {
            Collection<Goal> goals = m.getGoals();
            for(Goal g : goals) {
                if(g.equals(goal)) {
                    missionContainingGoal = m;
                }
            }
        }
        if(missionContainingGoal == null) {
            throw new MoiseException("Goal not found in FS");
        }
        Collection<Norm> norms = os.getNS().getNorms();
        Role role = null;
        for(Norm n : norms) {
            if(n.getMission().equals(missionContainingGoal)) {
                role = n.getRole();
            }
        }
        if(role == null) {
            throw new MoiseException("The goal is not associated with any role");
        }
        return role;
    }
    
    private Collection<Goal> getRoleGoals(Role role) {
        Collection<Goal> goals = new ArrayList<>();
        Collection<Norm> norms = os.getNS().getNorms();
        for(Norm n : norms) {
            if(n.getRole().equals(role)) {
                Mission m = n.getMission();
                goals.addAll(m.getGoals());
            }
        }
        return goals;
    }

    @Override
    public Element getAsDOM(Document document) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getStyleSheetName() {
        return "noplAccountabilityArtifactInstance";
    }

}
